import React from 'react';

class CartItem extends React.Component {
  render() {
    var { item } = this.props;
    return (
      <tr>
        <th scope="row">
          <img
            src={ item.product.image }
            alt={ item.product.name }
            className="rounded mx-auto d-block"
          />
        </th>
        <td className="align-middle text-center">{ item.product.name }</td>
        <td className="align-middle text-center">{ item.product.price }$</td>
        <td className="align-middle text-center">
          <div className="btn-group ml-3" role="group" aria-label="Basic example">
            <button
              type="button"
              className="btn btn-outline-info"
              onClick={ () => this.onUpdateQuantity(item.product.id, item.quantity - 1) }
            >-</button>
            <button type="button" className="btn btn-outline-info" disabled>{ item.quantity }</button>
            <button
              type="button"
              className="btn btn-outline-info"
              onClick={ () => this.onUpdateQuantity(item.product.id, item.quantity + 1) }
            >+</button>
          </div>
        </td>
        <td className="align-middle text-center">
          { this.showProductPrice(item.product.price, item.quantity) }$
        </td>
        <td className="align-middle text-center">
          <button
            type="button"
            className="btn btn-outline-danger btn-sm"
            onClick={ () => this.onRemoveInCart(item.product.id) }
          >
            <span className="fas fa-trash-alt mr-2" />Xóa
          </button>
        </td>
      </tr>
    );
  }

  showProductPrice = (price, quantity) => {
    return price * quantity;
  }

  onUpdateQuantity = (id, quantity) => {
    if(quantity > 0) {
      this.props.onUpdateQuantity(id, quantity);
    }
  }

  onRemoveInCart = (id) => {
    this.props.onRemoveInCart(id);
  }

}

export default CartItem;
