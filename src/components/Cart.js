import React from 'react';

class Cart extends React.Component {
  render() {
    var { children } = this.props;
    return (
      <div className="row">
        <h3 className="text-capitalize mx-auto mb-4">giỏ hàng</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th scope="col" />
              <th scope="col" className="text-center">Sản Phẩm</th>
              <th scope="col" className="text-center">Giá</th>
              <th scope="col" className="text-center">Số Lượng</th>
              <th scope="col" className="text-center">Tổng Cộng</th>
              <th scope="col" />
            </tr>
          </thead>
          <tbody>
            { children }
          </tbody>
        </table>
      </div>
    );
  }
}

export default Cart;
