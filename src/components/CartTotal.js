import React from 'react';

class CartTotal extends React.Component {
  render() {
    var { cart } = this.props;
    return (
      <tr>
        <td colSpan="4"></td>
        <td colSpan="2" className="d-flex justify-content-between">
          <h5 className="text-capitalize">tổng tiền:</h5>
          <strong>{ this.showCartTotal(cart) }$</strong>
        </td>
        <td className="align-middle text-center">
          <button
            type="button"
            className="btn btn-primary btn-sm"
          >
            Thanh Toán<span className="fas fa-chevron-right ml-2"></span>
          </button>
        </td>
      </tr>
    );
  }

  showCartTotal = (cart) => {
    var result = 0;
    if(cart.length > 0) {
      for (var i = 0; i < cart.length; i++) {
        result += cart[i].product.price * cart[i].quantity;
      }
    }
    return result;
  }
}

export default CartTotal;
