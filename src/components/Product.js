import React from 'react';

class Product extends React.Component {
  render() {
    var { product } = this.props;
    return (
      <div className="col mb-4">
        <div className="card h-100">
          <img
            src={ product.image }
            className="card-img-top"
            alt={ product.name }
          />
          <div className="card-body">
            <h5 className="card-title">{ product.name }</h5>
            <div className="rating">
              { this.showProductRating(product.rating) }
            </div>
            <p className="card-text mt-2">
              { product.description }
            </p>
            <div className="cart-footer d-flex justify-content-between align-items-center">
              <h5>{ product.price }$</h5>
              <button
                type="button"
                className="btn btn-outline-primary btn-sm"
                onClick={ () => this.onAddToCart(product) }
              >
                <i className="fa fa-shopping-cart mr-2" />Add To Cart
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  showProductRating = (rating) => {
    var result = [];
    for(let i = 0; i < rating; i++) {
      result.push(<i key={i} className="fas fa-star" />);
    }
    for(let i = 0; i < 5 - rating; i++) {
      result.push(<i key={i + rating} className="far fa-star" />);
    }
    return result;
  }

  onAddToCart = (product) => {
    this.props.onAddToCart(product, 1);
  }

}

export default Product;
