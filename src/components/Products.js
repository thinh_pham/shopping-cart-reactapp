import React from 'react';

class Products extends React.Component {
  render() {
    var { children } = this.props;
    return (
      <div>
        <div className="row">
          <h3 className="text-capitalize mx-auto mb-4">danh sách sản phẩm</h3>
        </div>
        <div className="row row-cols-1 row-cols-md-3">
          { children }
        </div>
      </div>
    );
  }
}

export default Products;
