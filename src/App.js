import React from 'react';
import ProductsContainer from './containers/ProductsContainer';
import CartContainer from './containers/CartContainer';

class App extends React.Component {
  render() {
    return (
      <main className="container mt-3">
        <ProductsContainer></ProductsContainer>
        <CartContainer></CartContainer>
      </main>
    );
  }
}

export default App;
