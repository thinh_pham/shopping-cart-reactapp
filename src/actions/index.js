import * as types from './../constants/ActionTypes';

export const addToCart = (product, quantity) => {
	return {
		type : types.ADD_TO_CART,
		product,
		quantity
	}
};

export const updateQuantity = (id, quantity) => {
	return {
		type : types.UPDATE_QUANTITY_PRODUCT,
		id,
		quantity
	}
};

export const removeInCart = (id) => {
	return {
		type : types.REMOVE_PRODUCT_IN_CART,
		id
	}
};

export const changeMsg = (message) => {
	return {
		type : types.CHANGE_MSG,
		message
	}
};