import React from 'react';
import Products from './../components/Products';
import Product from './../components/Product';
import { connect } from 'react-redux';
import PropTypes from 'prop-types/prop-types';
import * as actions from './../actions/index';

class ProductsContainer extends React.Component {
  render() {
    var { products } = this.props;
    return (
      <Products>
        { this.showProductList(products) }
      </Products>
    );
  }

  showProductList = (products) => {
    var { onAddToCart } = this.props;
    var result = null;
    if(products.length > 0) {
      result = products.map((product, index) => {
        return  <Product
                  key={ index }
                  product={ product }
                  onAddToCart={ onAddToCart }
                ></Product>;
      });
    }
    return result;
  }

}

ProductsContainer.propTypes = {
  products : PropTypes.arrayOf(
    PropTypes.shape({
      id : PropTypes.number.isRequired,
      image : PropTypes.string.isRequired,
      name : PropTypes.string.isRequired,
      price : PropTypes.number.isRequired,
      rating : PropTypes.number.isRequired,
      description : PropTypes.string.isRequired,
    })
  ).isRequired,
  onAddToCart : PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    products : state.products
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onAddToCart : (product, quantity) => {
      dispatch(actions.addToCart(product, quantity));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductsContainer);
