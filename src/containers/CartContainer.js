import React from 'react';
import Cart from './../components/Cart';
import CartItem from './../components/CartItem';
import CartTotal from './../components/CartTotal';
import { connect } from 'react-redux';
import PropTypes from 'prop-types/prop-types';
import * as actions from './../actions/index';
import { CART_EMPTY } from './../constants/Message';

class CartContainer extends React.Component {
  render() {
    var { cart } = this.props;
    return (
      <Cart>
        { this.showProductList(cart) }
        { this.showCartTotal(cart) }
      </Cart>
    );
  }

  showProductList = (cart) => {
    var {
      onRemoveInCart,
      onUpdateQuantity
    } = this.props;
    var result = <tr><td colSpan="6" className="text-center">{ CART_EMPTY }</td></tr>;
    if(cart.length > 0) {
      result = cart.map((item, index) => {
        return  <CartItem
                  key={ index }
                  item={ item }
                  onRemoveInCart={ onRemoveInCart }
                  onUpdateQuantity={ onUpdateQuantity }
                ></CartItem>
      });
    }
    return result;
  }

  showCartTotal = (cart) => {
    var result = null;
    if(cart.length > 0)
      result = <CartTotal cart={ cart }></CartTotal>
    return result;
  }

}

CartContainer.propTypes = {
  cart : PropTypes.arrayOf(
    PropTypes.shape({
      product : PropTypes.shape({
        id : PropTypes.number.isRequired,
        image : PropTypes.string.isRequired,
        name : PropTypes.string.isRequired,
        price : PropTypes.number.isRequired,
        rating : PropTypes.number.isRequired,
        description : PropTypes.string.isRequired,
      }),
      quantity : PropTypes.number.isRequired
    })
  ).isRequired,
  onUpdateQuantity : PropTypes.func.isRequired,
  onRemoveInCart : PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    cart : state.cart
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateQuantity : (id, quantity) => {
      dispatch(actions.updateQuantity(id, quantity));
    },
    onRemoveInCart : (id) => {
      dispatch(actions.removeInCart(id));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer);
