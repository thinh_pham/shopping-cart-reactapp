import * as types from './../constants/ActionTypes';
import * as msg from './../constants/Message';

var initialState = msg.CART_DEFAULT;

const products = (state = initialState, action) => {
	switch(action.type) {
		case types.CHANGE_MSG:
			return action.message;

		default:
			return state;
	}
}

export default products;